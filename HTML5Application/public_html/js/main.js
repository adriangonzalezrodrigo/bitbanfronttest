var arrayItems = {};
var filterItems = ['0-9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Z'];

/**
 * 
 */
$(document).ready(function () {
    // JSON "response"
    var response = JSON.parse(jsonResponse);
    var items = response.items;

    addItemsToView(items);
    console.log(arrayItems);

    setViewFilters(items);

    setViewFiltersListeners();
});

/**
 * Function to show items
 * 
 * @param {type} items
 * @returns {undefined}
 */
function addItemsToView(items) {
    var galleryContainer = $("#idGalleryContainer");
    // Add items to window
    items.forEach(function (item) {
        arrayItems[item.id] = item;
        var divColContainer = '<div class="col-6 col-sm-6 col-md-3" id="container_item_' + item.id + '">';// div Col Container
        divColContainer += '<div class="cardContainer" style="padding: 5px;">';// div
        divColContainer += '<div class="centeredBottom">';// container of card items
        divColContainer += '<span class="cardItem"><b>' + item.nick + '</b></span> <br/>';// nick
        if (typeof (item.name) !== 'undefined') {
            divColContainer += '<span class="cardItem">' + item.name + '</span> <br/>';// name
        }
        if (typeof (item.surname) !== 'undefined') {
            divColContainer += '<span class="cardItem">' + item.surname + '</span> <br/>';// surname
        }
        if (typeof (item.role) !== 'undefined') {
            href = "#";
            if ((typeof (item.roleLink) !== 'undefined') &&
                    ((typeof (item.roleLink.URL) !== 'undefined'))) {
                href = item.roleLink.URL;
            }
            divColContainer += '<a href="' + href + '" class="role">' + item.role + '</a>';// role
        }
        divColContainer += '</div>';//end container card items
        if (typeof (item.imageURL) !== 'undefined') {
            divColContainer += '<img src="' + item.imageURL + '" class="img-fluid" style="max-width: 100%; height: auto;" alt="Image">';// img
        } else {
            divColContainer += '<img src="img/fakeimageplayer.jpg" class="img-fluid" style="max-width: 100%; height: auto;" alt="Image">';// img
        }
        divColContainer += '</div>';// end div
        divColContainer += '</div>';// end Col Container
        galleryContainer.append(divColContainer);
    });
}

/**
 * Function to set view filters
 * 
 * @param {type} items
 * @returns {undefined}
 */
function setViewFilters(items) {
    var optContent = '<option value="." data-filterstring="' + '.' + '">Ver todos</option>';
    $("#selectFilter").append(optContent);

    // Add items to filter in li ul and select
    $.each(filterItems, function (index, filterStr) {
        classLiItem = "filterNoItems";
        items.forEach(function (item) {
            if (item.nick.toUpperCase().match("^" + filterStr)) {
                classLiItem = "filterHasItems";
                return false;
            }
        });
        var liContent = '<li data-filterstring="' + filterStr + '" class="' + classLiItem + '"><span>' + filterStr + '</span></li>';
        $("#ulFilter").append(liContent);
        $("#ulFilterVertical").append(liContent);

        optDisabled = "disabled";
        items.forEach(function (item) {
            if (item.nick.toUpperCase().match("^" + filterStr)) {
                optDisabled = "";
                return false;
            }
        });
        var optContent = '<option value="' + filterStr + '" data-filterstring="' + filterStr + '" ' + optDisabled + '>' + filterStr + '</option>';
        $("#selectFilter").append(optContent);
    });
}

/**
 * Function to set filters handlers
 * 
 * @returns {undefined}
 */
function setViewFiltersListeners() {
    // li listener
    $("#ulFilter li").click(function (e) {
        var strToFilter = $(this).attr('data-filterstring');
        filterBy(strToFilter);

        // Set option selected in select filter
        $("#selectFilter option").each(function () {
            $(this).removeAttr("selected");
            $(this).prop("selected", false);
        });
        $("#selectFilter option[value='" + strToFilter + "']").attr("selected", "selected");
        $("#selectFilter option[value='" + strToFilter + "']").prop("selected", "selected");
    });
    // Select listener
    $("#selectFilter").change(function () {
        var element = $("option:selected", this);
        var strToFilter = element.attr("data-filterstring");
        filterBy(strToFilter);
    });
}

/**
 * Function to show or hide items that matches with some pattern
 * 
 * @param {type} str
 * @returns {undefined}
 */
function filterBy(str) {
    var regex = '\^' + str + '';
    $.each(arrayItems, function (id, item) {
        strItemId = "container_item_" + id;
        if (item.nick.toUpperCase().match(regex)) {
            $("#" + strItemId).show();
        } else {
            $("#" + strItemId).hide();
        }
    });
}